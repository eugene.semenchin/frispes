$(document).ready(function() {
  initSlider();
  cardSlider();
  feedSlider();
  workSlider();
  initFacilitiesSliders();
  initBurger ();
})

function initSlider () {
  const swiper = new Swiper('.slider-section', {
    loop: true,
    pagination: {
      el: '.swiper-pagination',
    },
    clickable: true,
    autoplay: {
      delay: 3000,
    },
  });
}
function cardSlider () {
  var swiper = new Swiper('.swiper-card', {
    slidesPerView: 1,
    spaceBetween: 0,
    slidesPerGroup: 1,
    loop: true,
    loopFillGroupWithBlank: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.card-slider-nav .slide-next',
      prevEl: '.card-slider-nav .slide-prev',
    },
    autoplay: {
      delay: 3000,
    },
    autoplay: true,
    breakpoints: {
      // when window width is >= 480px
      480: {
        slidesPerView: 1,
        spaceBetween: 20,
        // autoplay: true
      },
      // when window width is >= 992px
      768: {
        slidesPerView: 2,
        spaceBetween: 20,
        autoplay: false
      },
      // when window width is >= 992px
      992: {
        slidesPerView: 3,
        spaceBetween: 30,
        autoplay: false
      }
    }
  });
}
function feedSlider () {
  var swiper = new Swiper('.swiper-feed', {
    slidesPerView: 1,
    spaceBetween: 30,
    slidesPerGroup: 1,
    loop: true,
    // loopFillGroupWithBlank: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.feed-slider-nav .slide-next',
      prevEl: '.feed-slider-nav .slide-prev',
    },
    autoplay: true,

    autoplay: {
      delay: 3000
    },

    breakpoints: {

      // when window width is >= 992px
      768: {
        slidesPerView: 1,
        spaceBetween: 20,
      },
      // when window width is >= 992px
      992: {
        slidesPerView: 2,
        spaceBetween: 30,
        autoplay: false,
      },
      1200: {
        slidesPerView: 3,
        spaceBetween: 30,
        autoplay: false,
      }
    }


  });
}
function workSlider () {
  var swiper = new Swiper('.swiper-work', {
    slidesPerView: 1,
    spaceBetween: 0,
    slidesOffsetBefore: 0,
    // slidesPerGroup: 1,
    loop: true,
    // loopFillGroupWithBlank: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.work-slider-nav .slide-next',
      prevEl: '.work-slider-nav .slide-prev',
    },

    autoplay: {
      delay: 3000,
    },
    autoplay: true,

    breakpoints: {

      // when window width is >= 768px
      768: {
        slidesPerView: 1,
        spaceBetween: 10,
      },
      // when window width is >= 992px
      992: {
        slidesPerView: 2,
        spaceBetween: 20,
        autoplay: false,
      },
      // when window width is >= 1200px
      
      1100: {
        slidesPerView: 3,
        spaceBetween: 30,
        slidesOffsetBefore: 0,
        autoplay: false,
      },
      1150: {
        slidesPerView: 3,
        spaceBetween: 30,
        slidesOffsetBefore: 50,
        autoplay: false,
      },
      1200: {
        slidesPerView: 3,
        spaceBetween: 30,
        slidesOffsetBefore: 100,
        autoplay: false,
      },
      1250: {
        slidesPerView: 3,
        spaceBetween: 30,
        slidesOffsetBefore: 200,
        autoplay: false,
      },
      1300: {
        slidesPerView: 3,
        spaceBetween: 30,
        slidesOffsetBefore: 250,
        autoplay: false,
      }
    }
  });
}

function initFacilitiesSliders () {
  var left = new Swiper('.facilities-sliders .slider-left', {
    slidesPerView: 10,
    direction: 'vertical'
  });
  var right = new Swiper('.facilities-sliders .slider-right', {
    slidesPerView: 1,
    thumbs: {
      swiper: left,
    },

    autoplay: {
      delay: 1000
    },

    breakpoints: {
      768: {
        autoplay: false
      }
    },
  });
}

function initBurger () {
  const hamburger = document.querySelector('.hamburger');
  const nav = document.querySelector('.nav');
  const links = document.querySelectorAll('.nav li');

  hamburger.addEventListener('click', () => {
    nav.classList.toggle('open');
    links.forEach(link => {
      link.classList.toggle('fade');
    });
  });
}


